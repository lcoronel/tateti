package src.test.java;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import src.main.java.Tablero;

/**
 * Created by otto on 22/03/17.
 */
public class TestTablero {

    private Tablero tab;

	@Before
    public void setUp(){
		tab = new Tablero();
    }
	
	@After
	public void tearDown(){
		this.tab.wipeBoard();
	}
    
    @Test
    public void testCrearTableroTodasLasPosicionesEstanVacias(){
    	assertFalse(tab.hayganador('x','o'));
    	for(int x=0; x<2; x++){
            for(int y=0; y<2; y++){
               assert(this.tab.posicionVacia(x,y));
            }
        }
    }

    
    @Test
    public void testSePuedeMover(){
        assert(tab.posicionVacia(0,0));
        tab.poner('x',0,0);
        assertFalse(this.tab.posicionVacia(0,0));
    }
    
    @Test
    public void testDeIntegracion(){
    	this.tab.poner('x', 0, 0);
    	this.tab.poner('a', 1, 0);
    	this.tab.poner('b', 2, 0);
    	this.tab.poner('c', 0, 1);
    	this.tab.poner('d', 1, 1);
    	this.tab.poner('e', 2, 1);
    	this.tab.poner('f', 0, 2);
    	this.tab.poner('g', 1, 2);
    	this.tab.poner('h', 2, 2);
    	this.tab.printTablero();
    }

}
