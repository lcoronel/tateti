package src.main.java;



/**
 * Created by otto on 22/03/17.
 */
public class Tablero {

    private Character[][] matrix;

    public Tablero(){
    	this.wipeBoard();
    }
    
    public boolean posicionVacia(int x, int y) {
    //Dice si la posicion actual es vacia
    	return (this.matrix[x][y] == ' ');
	}
    
    public void wipeBoard(){
    	this.matrix = new Character[3][3];
    	for(int x=0; x<=2; x++){
            for(int y=0; y<=2; y++){
               this.poner(' ', x, y);
            }
        }
    }
    
    public boolean ganador(char jugador){
        boolean completado = (matrix[0][0] == jugador && matrix[1][1] == jugador && matrix[2][2] == jugador) ||
                matrix[0][2] == jugador && matrix[1][1] == jugador && matrix[2][0] == jugador;
        for(int x=0; x<=2; x++){
            completado |= matrix[x][0] == jugador && matrix[x][1] == jugador && matrix[x][2] == jugador;
            completado |= matrix[1][x] == jugador && matrix[1][x] == jugador && matrix[2][x] == jugador;
        }
        return completado;
    }

    public void poner(char c, int x, int y) {
        matrix[x][y] = c;
    }
    
    public void printTablero(){
    	for (int fila=0; fila < matrix.length; fila++){
    	    System.out.println(matrix[fila][0] + " " +  matrix[fila][1] + " " + matrix[fila][2]);
    	    
    	}
    }

	public boolean hayganador(char x,char o) {
		// TODO Auto-generated method stub
		return this.ganador(x) || this.ganador(o);
	}
    
}
